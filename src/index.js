import React from 'react';
import ReactDOM from 'react-dom';
import { HeroesAPP } from './HeroesAPP';

ReactDOM.render(
    <HeroesAPP />,
  document.getElementById('root')
);
