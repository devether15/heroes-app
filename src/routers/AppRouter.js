import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
  } from "react-router-dom";
import { LoginsScren } from '../components/login/LoginsScren';
import { MarvelScreen } from '../components/marvel/MarvelScreen';
import { Navbar } from '../components/ui/Navbar';
import { DashboardRoutes } from './DashboardRoutes';

export const AppRouter = () => {
    return (
        <Router>
            <div>           
           
            <Switch>
               <Route exact path="/login" component={ LoginsScren} />

               <Route path="/" component={ DashboardRoutes } />
            </Switch>
            </div>
        </Router>
    )
}
